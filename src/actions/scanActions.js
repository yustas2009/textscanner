import { ADD_ITEM } from '../types';

export const addItemToCart = (data) => {
  return (dispatch) => {
    dispatch({ type: ADD_ITEM, payload: data });
  };
};
