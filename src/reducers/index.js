import { combineReducers } from 'redux';
import { scanText } from './restourantList';
// import orderReducer, * as OrdersSelectors from './orderReducer.js';

const rootReducer = combineReducers({
  scanText
});

export default rootReducer;
