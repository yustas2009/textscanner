import { FETCH_CATEGORIES_START, FETCHED_CATEGORIES, FETCH_CATEGORIES_ERROR } from '../types';


const initialState = {
  fetching: false,
  scannedTexts: []
};

export const scanText = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_CATEGORIES_START:
      return { ...state, fetching: true };
    case FETCH_CATEGORIES_ERROR:
      return { ...state, fetching: false, error: action.payload };
    case FETCHED_CATEGORIES:
      return { ...state, fetching: false, fetched: true, catgoriesList: action.payload };
    // case FETCHED_CATEGORIESPPP:
    //   return [...state, ...action.payload];
    default:
      return state;
  }
};
