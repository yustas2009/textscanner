import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Text, View } from 'react-native';
import { Container } from 'native-base';
import { addItemToCart } from '../actions/scanActions';

class MainScreen extends Component {
  addToCart = (item) => {
    this.props.addItemToCart(item);
  }

  render() {
    // const item = this.props.navigation.getParam('item', 'NO-ID');
    return (
      <Container>
        <View>
          <Text>Main screen</Text>
        </View>
      </Container>
    );
  }
}

// const styles = StyleSheet.create({
//   imgContainer: {
//     height: 230,
//     backgroundColor: 'powderblue',
//     alignItems: 'stretch'
//   },
// });

export default connect(null, { addItemToCart })(MainScreen);
